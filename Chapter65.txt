 1 Chapter 05
goon after their return to Lecta, the BloemehiEAJ bo*m
.
visited driving an Cverlazqa car. hey had come before; a l _
,. the chi dren had had rides, and it was very exciting. Low
Jim wanted to try i t , too. ”I'd buy only a Ford, not a new one.“ ie talked care a l l winter but dia nothing about i t .
IL tne scrin* I was six, and oecauee Kother did not pay much attention to Heerman or me, I could run through the pasture to Uncle John and Aunt Mau Jouse. It wasn‘t far, and her
arden path was well‐worn. This was a different place than
1
aunt Einnie's house. here vere three g i r l s almost a l l of myage.
haude, who was adopted; Anna a l i t t l e older than I; and Lisbet,
a little youner. They had large trees to climb, a croquet set
in the yarq, and always boots. I loved to stay to eat at Uncle John‘s; we always had suon l i f e l r conversations that were ex‐ citing, and he had lovely, big stallions nrith farnous names I could not remember until I grew older. He a so had a big, round dining table with a revolving smaller round on which the ketchup,
mustard, s a l t and pepper, su;ar and cream, and bread and butter stood. If you wanted something, you gave the rouno a p u l l or a ~-ush, and there it was. We ICVed the washhouse next to a b i g tree; if one was darin;, the tree gahe access to the roof of
t h e waehshed.
Wealso met in our willow grove, Iick‘s Julia and Peter,
too; and we each had a Sp 0 1 a tree; Johnny's tr
favorite. He could clin1b it easily, and one b i g limb hafi no branches because we had pulled them o f f it was sliiper“, and

 we s l i d down it to the around. We loved the swale, too, in our
pasture where the frogs had
about it because we might not waée in i t .
Once I remember Aunt haude was s win;, and I said to Anna, ”I can stick a needle right into that little black hole in your eye.“ Did Aunt Kaude shout! “"es£ You'll make her blind!”
Later, when we were older, we ccnnived tc;ether to get
series of books. We loved the Tarzan and Jan: books. They “Gt
1
some, and acged 10? different ones of the same series Neal‐
ways spent time there in reaminfi. I had the Grace Harlowe series and they had the Eeedowbrook Girls; we would not ccndescend to the Eobbsey Twins, but we read a l l the Zane Grey and Alger books. :ince annie was taking lessons I r o m Joe Schuld and never missed
me, I'd ban; around u n t i l supper time and eat there.
heyday. Wewere always mystified