Chapter 28
the Sunday in beptember a t a l l young man walked up the aisle and sat down on the frOht bench. ”Who is that?” Julia nudged Mary.
“C‘, that is Jacobus Ten Cate from Michigan. Dc ycu want
to meet him? He is enmamed to a nirl in SaU' “ ‘ UL; V
Where he came from.“ Julia looked again at Cohus sittin"
absorbed in the minister's words. She like the oised bis he"d (J
on slender shoulders. The dark h a i r clung closely to h i s broad brow, and as he turned, h i s deep l i g h t eyes looked directly into her questioning gaze.
After the service Jane ssucily said, ”Cobus, I have a cousin here from Illinois. Want to meet her? This is Julia De Boer.“
”Sure.“ Julia's square l i t t l e hand was enveloped in h i s capacious one. The big, light eyes searched her face, lingered Ohthe mobile mouth and the golden wings of hair swept neatly
and smoothly back to the low knot on her white heck. “Haw I u
see you home next week?”
”Yes, I don't know; yes, you may." Julia gasped in exeite‐ ment. Kent week, why not tonight? “Kary, Why next week?” She was consumed w i t h wonder.
fiext week, next week Sunday night. He er had Julia been so attracted as by Jim. I like e erything about his looks she confided to herself
The days of that week passed sc slowly that Julia worked furiously at added tasks to keep her mind occupied. A letter from Jacques was a diversion b u t no treasure; it was r e a d once

 and laid aside with the others. She wrote a long letter to her brothers trying hard not to mention Jim. Her last sentence, howe e r , was, “I met a young man named Jacobus Te n Cate."
Sunday 9 ening Jim was at the Sioux Center Church aih. Julia didn't hear much of the young people's program,for her heart was thumping too loudly. After the ser ices Jim calmly appropriated Julia's arm and led her to his bug;y. He settled her carefully in one corner and covered her lap with a limht
\J
1‐ robe called a duster on which was embroidered a huge rose. he
got i n , arranged the robe over his knees, giving them a sense of intimacy and making the opening words even harder f o r Julia to manage.
“You have a fine horse. Is he yours?“
”‘7 .
i e s , and the buggy is mine also. It has side curtains in
case of rain or snow and ieinglaes to fasten over the front. giddapl Where do you live?”
Crestfallen, Julia wondered if he would drive her directly home, but instead he drove the horse to a side road and let him choose his own gait while he talked to her. His words came out in a memorized fashion.
”I'm a Hollander from Apeldoorn, Xetherlands, and liVed
in Holland, MichigaL, then in Eaugetuck, and now in Orange City
where I work f o r my eldest sister's husband, John Bloemendaal.
I amtwenty-three years old. Before I left Holland, kichigan,l
1‘
became engaged to Melinda ye Vries, but I wrote her Monday night
that I could not consider marrying her after I saw you. T3hnis may seem a cruel thin; to do, but Eelinda never did love mevery
much and elreadvhas been c’oih': out With her father‘s hired man. u UK)

 ‐
I belongto the Christi"n Reformed Church and amVery interested
in you.”
Julia listened aid watched this very strange young man with her mouth slightly open. 50 this was romance. He certainly didnft waste any time in eettin; forth his program.
”Would you like to t e l l meabout yourself?“ he asked. “Did you learn your speech 10y heart?“ asked Julia flippan
[1‐1 v“..' H flo;plve me,
when she saw the hurt look on his
face.
Julia told her story in detail, and to the young man
listening it was a saga of hardship a.d triumph of love both
human and divine.
llfl
g o d l e d mehere," she concluded. ”and now
have met you. Who can say Go ’5 hand is not in this?”
Hr)!
.PL’J moment I saw you, I knew,“ said Jim
were the one. Shall I take you home now? It i
He l e f t her ccurtecusly at the door and drove rapidly away.
Julia steed for a moment in the flooding moonlight, lifted her face to tLe stars, and breathed the age‐cld prayer of lovers,
”haLe him love me, Lord.” The 0
brought h e r back to mundahe kindled which was to burn and warmth in the
heartache.
sniffed at her ankles and ai.LalZ‘S. Still the flame had been
steady glow, shedding l i g h t sofulloflonelinescand
\n-GF '
