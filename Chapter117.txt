Chapter 117
In August t h e same members of t h e board asked me to teach again. Alida Haan would teach the third and fourth grades, and for half days I had the fifth and sixth grades; Mr. Belt had
the seventh and eighth. For this I received fi f t y dollars a week. The boys were a l l happy about having more money, but I had to crush their hopes. "No, first things first; the money must go to pay o f f the bank. Wecannot spend if we owe money; this money
w i l l cover our bank loan.“
Jimmy and Tommy were old enough to go to school with me, and
Herb was starting high school. Wel e f t with Thomas at seven thirty; I returnedat twelvebi streetcar, walked from there,
and stopped at Ed's house where Mother Chambery was waiting f o r me. I can see her still with either her Bible or the Psalter on her lap. After we had had tea, I went home. Mother Chambery had gtayed at Ed's house since Edna's death taking care of him and the four children; Jeanne was in the eighth grade, the younger girlswentto LandingRoadSchoolasallof theChamberychildren
had done, and Chuckie was s t i l l at home.
After Christmas Alida Haanwho had taught the lower grades
did not return. Then I taught the fourthand fifth grades in
the morning while Dorothea Dys taught the lower grades until noon. For the afternoon I took the lower grades, and Mr. Belt took hy classes. It was a l l very confusing and f a r more work be‐ cause we had to prepare different schedules. I now had the car and drove Thomas to the carline, brought Alma and Frank home
with our boys, and had to meet Thomas again at the carline. In

 the mornings the boys had to make their beds, dust the stair‐ case, and take upthe little rugs and shake them out.
I prepared a l l lunches the evening before. It was a busy schedule with not much time f o r reading, but we never missed the evening Bible Classes or Society Meetings. God in His wisdom giVes the young much strength and health.
Weusually had our dinner hour early, at least by six o'clock. Alma and Frank would look longingly at the food, but Frances wanted to serve a real dinner, and for that reason, I always
gave them a little and some dessert to tide them over and not
feel like outcasts. Louis usually came for them at seven.
