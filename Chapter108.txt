 Chapter 108
May 23, 1933, we received a telegram summoning us home. My mother had had a serious heart failure. myfamilw sent fi f t y dollars f o r our expenses to drive down and back. Thomas immedi‐ ately took our Whippet to Johhvan't Hof, his cousin, for repairs and bought a license f o r thirteen dollars; that same day we set
out.
Herb would stay with Grandma Chambery, and l i t t l e Tommy would sleep in a swing our friends Fred and Jessie Steensma had loaned us. It was hung from the frame of the roof. I nursed him so there was no problem there. If the Lord had not been on our side, wewould never have made that fifteen~hundred‐mile t r i p
without incident.
F i f t y dollars seemed a fortune to us. Wecould sleep in tourist homes for a dollar a night; gas was thirty or thirty-five cents a gallon, and meals averaged thirty-five cents for a dinner.
Tommy slept practically the whole way, and when he was awake, I nursed and played with him; he wasn‘t a crying child.
When we arrived at home, everyone wanted to see our precious boy. Mother had improved and was her usual s e l f although my s i s ‐ ters treated her l i k e a piece of rare china. Annie had beautiful children due to Frank, although Stanley looked l i k e a Ten Gate,
and when she saw Tommy, she commented in her usual frank way, "He isn't as good looking as our children, but he is the most
charming baby I ever saw.”
My sisters were appalled at my clothes. I had been married three years and hadn't bought any new clothes; I couldn't. My
hairdresser sister Carrie gave mea wind-blown permanent, and May Bellesaid she would sew f o r me. I had been wearing Hoover

 dresses or aprons which cost fi f t y cents apiece, one green dotted in white and the other pink and even more ugly. I had bought them f o r the t r i p because I could fiurse Tommy easily in them.
May Belle made metwo silk dresses, on rose colored with a bib that had a row of covered buttons on each side, fitted per~ fectly, and looked beautiful. The other was a blue silk with ruffles which I could open for nursingTommy. Thesewere for best wear, and then she made three cotton dresses f o r d a i l y wear. Now both body and headwere more like the g i r l Thomas had married. Myfifty-cent dresses were in the rag bag; at least, that is
what I suspected.
The tires oh the Whippet were not trustworthy, and Herman found, he said, five good tire for five dollars. Wecould pay
it from our fi f t y dollars, but wenever really believed that f1ve~dollar story. Just the other day Howard Hughes came to see Thomas and took him to see a rejuvenated Whippet from the thirties.
He said it looked like new. I told him that was what we had had for our marriage t r i p and the car we drove to see Mbther and the family.
It was a pleasure to be with my own family and to run across the road to Aunt Dora and Uncle Doctor and to mysisters. A l l of the aunts invited us to dinner, and we enjoyed their love and
the abundant meals. In Minnesota and Iowa one could be very poor, but never in the way of food.
After ten days we decided to return and leaving with canned f r u i t , new clothes, and loving memories we routed our way to see Julia and Jake and their daughter Amaryllis 1n Leighton, Iowa. From there wedrove to Chicago, Illinois, to make a stop at

 Evergreen Park where my oldest nephew, Carrie's son, lived. We had stopped there also on our wedding t r i p so Thomas knew them.
In the first years of our marriage, we had few pleasures except the hospitality of friends in picnics, toboggan parties, nutting, swimming, church, and Bible classes; now Thomas wanted to stop f o r the World's Fair. Marvella cared f o r Tommy that day, and weneeded only a few cents for carfare and a dollar each for entrance fees. Wespent twenty-five cents each f o r a sandwich and coffee, and I bought a l i t t l e boxed wagon with rubber-tired
wheels f o r a quarter f o r Carla Ruth, Marvella and Peter's child. Wesaw a l l the free exhibits, at least Thomas did; I spent part of the time sitting on steps if I could find a convenient place.
Wefound our streetcar at t w i l i g h t and came to Evergreen Park where a man saw us at the bee stop and asked, "Where are you going?“
"To Peter Kooiman's barber shop."
"Oh, I know that place; hop in." A l l day had been God's goodness to us, and it was seen again in this providential ride.
The next day weleft for Rochester, over six hundred miles awey; wedid it in two days arriving very tired and with gifts
for Herbfrom the family. When Marthasaw mynew dresses, she said, "Now, you need new shoes, too; and she brought me a very beautiful pair for $2.50 which also came out of the fifty dollars. She worked at Reeds shoe factory and could buy shoes that had
minor defects. Over the years she kept mein shoes until she lost her place because Reeds closed.
