 Chapter 41
Jan returned to the U.&.A. to h i s sweetheart Keude and married her. She had never been more than twenty miles from home before, and now she mo ed to mid‐hmerica. She relates that even though she was a bride of only thirteen days Nick
3
and John sat in froniof the two-seater buggy, and She sat by
herself in back, dreading the ordeal of meeting th' paragon of a l l virtues, Julie. Inese brothers never lost their great love for their sister.
Rich's bride was already livinv with Julia anfl
thewr three children. Talk about close QuarterQ-‐th s wasn't it in larch of 1897.
While these marriagea had taken place, B i l l y , Herman,
t ship calling around hIrica and through
the Suez Cana- t
u went onto Italy and the Netherlande
nd then to america. :verywhere Lora picked up treasures and later
decorated her early Victorian home in q u c r t o x Fur rugs, class
boxes, a huge CStFiCd e43, and other curiosities aroused the Interest of the children. I never hear“ any of them refer to the African t r i p to us, but none 0 it was ever forgotten.
Uncle had a two‐carat diamond r i n g ; aunt Dora had '
a ai mzcne,
many other rings, and also African jewelry. Hermanus, the tallest
of a l l the men,had
e er achieveo; not even Uncle Bill?‐ ‐ i n him it seemed to be
Which we never saw in our other uncles. wen+ oack to mee1031 school and became the hesi Of
,.
(1 pelee and bearing that none of the ctherq
go wizere Cousl?
in a case of eggs,
Julia De Jong later

 sure that Uncle Billy would not charge for the operation.
Herman and Lore, of course, had stop;ed in Chicago to
.
v i s i t her people, and Herman went to Dalton ano Harvey to see
his old friends there; he came ahead of Dora to buy a house and
set up his business in Edgerton. Affectionately he said to Julia,
”You and Lily look so much alike, but Lily has the saddest face
I haqe ever seen; withal she is s t i l l beautiful.”
ll‐ 1
1 8 S r } poor, Herman, and is he cruel to her?“
"Ees, she is very poor. hlbert earns nothing, but he isn't cruel either. I think he is afraid of her; she is so dignified and calm and kind to him. Her daughters are a great comfort to her and give her money from what they earn. They are handsome
irls and educated, and will one day have good positions. Tahte Cato still lives, butnot for long.”
now is Father heeter?“
”Nell, he is getting older and quieter; he no longer
Grinhs anu has gone back to the church. He was glad to see
Wewere prepared to help him, but when he
horsewhipped and hated us, ve
.1d as little for him as we
1.
I
could, which wasn't r iVsht either because we had promised Rother. He often tells us little things about her.“
Jim asked, ”Wouldn't you like an office here? A l l these peoyle would l i k e a Dutch doctor."
”Well, not here, but in Edgerton. Dora would like that better. I ' l l go there today. May I use you wheél?”
”I'll go with Herman; I have a wheel, too. Jim and I ride often in the summer evenings,” said Julia.

 ”:he's just a g i r l yet,” thought Jim
on the marvelous kin and lcving the lithe figure proudly erect s t i l l after endless tasks an: backbreeking work.
expanse of his work. he was old and readv to sell his practice and good will, end then Julia pt n ed to a white fictorian house with a raised piazza on three sides. Would Dora like that hcuse? Julie thought it a palace when she compared it to two rooms f o r three families. They went through the house, and Herman put a substantial deposit on i t , but that too was subject to Dora's
acceptance.
”Oh, ierman, how wonderfully God has directed our affairs
11
just as Kother said or thought when sne said we should stay to‐
gether. It took time, but G=d is never in a hurry.” Herman’s
reply was a smile and a hug.
-. 0
Carrie and Annie had 013 discussions about the brides; how
could both Aunt Minnie and aunt Raude be so beautiful' each had 5
h e r favorite in t h i s assessment. They loved to look at the wed‐
1
ding pictures cf DOt; pairs and never could really agree; one
day one was more beautiful and the next day the other. Finally,
f‘r
warrie said, ”Cf course, they can't help being beautifu
; very‐ one that comes from Vhioa;o or near it is beautiful. hunt Dora
Will be, too.”
Annie usually believed Carrie knew e erythih“ but this “U5
time she doubted and said, “But Lcther came from near Chicago,
w
and she is not oeautiful. ispecially now with a teeth out and
1
that ran around h e r head f o r neuralgia.”
dmiring the happy flush

 ”Of course, not,“said Carrie,
yevrs, and then it i hard to etaun‐ oeautiful. But go matter,
\ Iloveherbestofall,exenwiththatre:aroundherhead.
And Aunt Dora was beautiful, EWeet, and youthful. A l l the ccusihs loved her a l l through the years.
Both aunts in the two rooms were pregnant now, and the rooms grew smaller as they Urew bigger. I can't imagine how they managed with no quarrels. It was getting warmer, too, and the two rooms upstairs were unbearablv hot. No two Cinderellae
ever had worse accommodations.

