Chapter 30
The summer time waned, and harvest time drew near. Jim
worked u n t i l late each dew and e wrew aaunt and hollow eved J5{.2vu
from O'erworh. Together they shocked the grain, Julie going barefoot to save her one p a i r of oes and pulling her skirts up as high as Jim would let her. The stubble scratched her ankles until they were red and sore. After Jim f e l l asleep she bathed them in vinegar and warm water and stumbled i n t o
bed too tired to think.
one night when the shockingwas almost finished, they
went back after t i e Chores were done to work under the huge harvest moon. When they stopped to eat t h e i r basket supper,
the beauty of the hiéht held esen the tired toilers spellbound. The moon was a shining torch f o r every shock of grain and the
*lue home was sprinkled with stars.
Julie quoted softly, ”The heaven deciare the 31 ry of God,
irmament showeth f o r t h H i s handiwork.” J i m continued, ”Thou visited the earth and watereth i t . Thou greatly enriched
it with the river of God which is f u i i of water. Thou preparest them o e r “ when thou provided for i t . Thcu waterest the ridges thereof abundantly; thou settest the furrews thereof, thou
maxest it soft with showers, thou blesseth the springing thereof. Thou arcinest the year with Qaedness, and thy patls drop fatness.“
“Jot sorry, Jim. It has been wonderful to Lave a home
with you.” Choosin; a shcch in the path of the moonlight, they leaned back luxuriantly and ate their cold snack. ”Let’s sleep
‐
here tonight,“ beg;ed Julie. ”It is warm and sobeautiful.”

 “All ri;ht, but it gets col. toward morning especitlly, and the ground gets ,ery hard.”
”de'll fi x a bed of shocks and use the blanket in the
blanket, and f e l l asleep under the benign moon. Jim was awake
‘
or a while and thou ht how wonderful to hate a wife whc thou ht
UV
elee‘in; under a harvest moon was an adventure.
Biue bv side they worked, bundled the fodder, and made
the barn snug f o r their two cows and two horses. One night as the last field was completed, Jim said, ”I'll hitch the horse to the plow and take it home before it snows emu rains to make it rusty. Doyou think you can pull the light wagon home?"
”Surely I can do that,” answered h i s wife.
”Watch the shafts 0n the h i l l though. Don't drop them.” Julia started off first, the light wagon rolling along
easily. when she reached the h i l l , the wagon was gently pushing her to greater speed until she was running as ‘ast as she coulu, but her long skirts impeded every step. Laughing and
for breath, s t e raced on, the wagon behind her in pursuit.
hearing her run end the rattle of the iron rims, Jim called, “Lean back, lean back,” but Julia heard nothing but the grindin¢ of the wheels and the whistlin; of her breath. at last the gate.
The h i l l was past and the slope gradual. The wagon stopped, and Julia sat by the wayside almost crying from the pain of her stone‐stubbed toes.
”How at least I know I can still run," she told Jim cheer‐
fullV' I was a little frightened that I might fall and the wagon d3
sets cold. Ch, let’s!” U
wagon when it
Together they made a high soft bed of shocks, got the

 run Ower me. I'llget the dinner ready w.nile you &0the chores.” The long hard winter was almost upon them. To Keep out drafts, the foundation was banked with manure and tied with
fine wire tc hold it in place. Windows were covered with white paper, and every crevice was packed with re
Butchering time was also at hand. The Bloemendeal's butchered on a huge scale. There were between ten and twelie Children and a l l the friends to feed, and Tente Rea set a good table.
Jim and Julie were asked to help, and f o r their astistance they received unetinted rewards: roasts, bacon, hams,
meat, bones for soup, and a few creche of lard. Jim built a lean-tc on the kitchen to hanb the meat to keep it free from birds, anu he suspended the larger pieces from he rafters on thin wire to keep them safe from field mice. This meat would soon he irozeh. Some of the meat was salted down in brine also.


