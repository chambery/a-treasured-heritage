Chapter 118
In May Elizabeth and Steve Palezeke came again and picked us up to go to Minnesota. Wehad a great time talking, and Steve had a good sense of humorxalso, Elizabeth was a licensed prac‐
t i c a l nurse at the hospital where Steve was, and they were now
married. The boys and I sat in the back seat. At fi r s t they were very well-behaved, but that didn't last. After cautioning them several times against quarreling, I said to Steve, "Stop the car, please." He did, and I said to the boys, "Get out, you can
walk,“ and wedrove on. Elizabeth and Steve thoughtmea very unethical and harsh mother.
Werode about a mile and over a h i l l hidden from the boys' sight; after a l i t t l e time we returned to two very woebegone
and chastened children. I went to them and took them in my arms. "Are you ready to obey?“
"Oh, yes, Mama, wewill." And they did. Wehad no trouble on the way home, either.
How happy we were to be in Edgerton again! Mary Lou was a cousin they hadn't known and Carla now liVed on a farm. Mother had renovated a garage,and with a l i t t l e addition had a very practical and gracious home. She could walk out to her garden
at groundlevel and had very l i t t l e housework.
The boys went to stay on the farm with Emma, and I slept on
the davenport bed in Mother‘s living room. Herman l e t me have his car to goto Leota and to visit myschool friends.
My sister Carrie came and saw the bald spot on Jimmy's head. She was a hairdresser ans said, "He will become entirely bald;
he has a fungus growth. The remedy is to burn it every day with

 white iodine.“ Every morning he came of h i s own accord f o r the treatment. I could see by the appearance of the spot that it must hurt him and asked him about i t .
"Of course, it hurts, Mother, but I don't want to be like Elisha and have the kids say 'Go up, you bald head'."‘
Emma had rented a pony and part for the boys, Carla, and Mary Lou. Uncle Doctor told us later that they had come into his office to get toilet paper. ”What for?" he had asked.
"To wipe the pony; hejust went to the toilet!" Uncle had watched while one boy held up the t a i l and the other one wiped. That shows how I t r a i n e d my children--cleanl
Wewent to Boyden to v i s i t Carrie and Gerrit; Peter was married and gone; Edna was married to Allen Xoungsma and with their child Dawn lived in a near-by town; we had lunch there. Edna had baked a delicious pineapple upside-down cake. When Carrie asked the boys how they liked their eggs, they said, "Round, not flat." Myfamily were so very, very good to us. Carrie gave me a permanent, and Gerrit cut the boys' hair.
A l l the aunts, especially Aunt Dora, loved having us there, May Belle sewed me a black siflkjumper. Annie and I had always been very close because she had mothered mea f t e r Johnnie's fleath. Mother told me she was going to build a chimney, a fake one on her roof. “But you can't do that-‐you'll get hurtl"
"No, I won't; I'm going to do i t , " and she did. Florence Huisken Youngsma had a movie camera, and so we can see it s t i l l .
Everything had changed though; f o r Emma and John l i v e d on John's father's farm; Annie had moved often-«I think in her
married l i f e she moved six times. A l l of my sisters were excel‐ lent cooks. Mother never taught any of us that. She herself had

 worked so hard in her youth that she didn't want her children to have that kind of life.
Both Annie and I suffered from the depression, but Emma lived on the farm and had plenty, and Carrie had lived in much the same style. Wewere a l l readers but chose different material. Herman loved to travel and read of places to go; Carrie read
love stories; Annie studied maps and newspapers, but music was what she liked best; Emma liked science and mystery stories.
She spent much time with her children and was the most loving. Herman loved a l l children as my father did.
The ride back to Rochester was var? pleasant; the boys behaved well, and in three days we were home again and ready
to work on the berry patch. Herb hated to pick them so we let him off to go to the golf links to earn spending money. Some‐ times he earned a dollar or a little more, often very little or nothing. Wewere inclined to call him laZy, but now as I think
of his heart surgery, he probably wasn't lazy at a l l but like myfather who had been called lazy due to his heart condition, too.
