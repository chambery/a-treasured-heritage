 Chapter 107
Monetary times remained the same. Welearned to conserve money, food, clothing, and heat. I never used the oven unless
I had a great many things to take at once. My bread from Mother Chambery‘s teaching was delicious. Wewalked to the library, and because we were not in the c i t y proper had to pay three dollars for a library card.
Herb was walking with us, and after we were several blocks from Winton Road, I said, "Oh, shoot, I heard Critics ice cream is fifteen cents a quart this week.“ Herb offered to run back for it; of course wehad to eat the whole quart because wehad no refrigeration of any kind, not even an ice chest.
When little Tommy was about three months old, hewas to be baptiZed with several other boys of the congregation: Gerrit Stam, Roy De Jager, and Bob Otto. Tommy was quite bald and had merry blue eyes, but he did drool more than five other kids put to‐ gether; otherwise he was perfect. Roy DeJager was a beautiful child, and while we were a l l in the Ladies Aid room waiting to
go down, h i s mother looked over the other children, and none of them compared with her boy in beauty. She kindly said to me,
"How does it feel to have a child that isn't good-looking?"
I answered her smartly, “I have never had another baby; so I can't tell.” We a l l thought he was a charming child, and he was, and he grew up that way.
Thomas had beautified our 'yard much in the years we lived in that little cottage. The first year we tried a vegetable garden, but the earth was so poor that Thomas made it into a circular garden of flowers holding a bird bath he had made. He also planted the banks of our lawn with iris. Webrought out

 flowers to the sick, often to Virginia Dys who had polio and
was in the children's hospital. Healso put a cement floor in
t h e basement where t h e washing machine stood. There never seemed to be anything that Thomas could not repair. From time to time
he had opportunities to put in plugs or to fix irons and lamps. A l l Jobs were g r i s t f o r h i s m i l l . He s t i l l can do these things, rejuvenate old,old furniture, and likes to do i t .
Wenever went onrelief. The dentist trusted usand sodid the landlord and the milkman. Wehad to pay the gas b i l l , or we would have had no stove or l i g h t . Everyone else had the same
disease--empty p o c k e t s . Wehad no telephone; what a blessing! Being poor isn't a l l bad.
