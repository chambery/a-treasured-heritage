Chapter 112
Thomas and I had the three children a l l sleeping in the same room. Herb had mygirlhood bed, and each little boy had a crib. On our anniversary in August Ontario Lahe is really warm and delightful. Wedecided to celebrate by going swimming. Our
bathing suits on under bathrobes, and towels to s i t on, we crept out quietly that evening. Wedidn't want Mother Chambery or Frans and Louis to hear us, so Thomas pushed the car out of the driveway while I steered; he didn‘t start the engine u n t i l
we were rolling down the street and f a r enoughfiaway that hey couldn't hear us.
Imagine our surprise when we returned about eleven to find Mother Chambery and Frans sitting on our front porch each with
a child in her arms. or course, Mother Chambery thought us crazy, but Frens conceded it was O.K. Somehow the boys had become aware that we were gone; their crying didn't awaken Herb, but did alarm our neighbors because they continued to howl. Neither did they
know where we were.
One Christmas Thomas had worked in McGurdy's store and seen
the b i g Santa Claus who quivered a l l over and every few minutes said, ”Let's make everybody happy. Ha, Ha!“ A f t e r the boys had
heard about i t , they wanted to go to see him. They enjoyed this, not knowing he was a weird fake, but we t o l d them to stay there while welooked for gifts for them. When wereturned, probably later than we should have, they were sitting on the curb, feet in the street, and holding their hands over their ears.

 That Christmas Rene Asbrooke asked Thomas if he'd l i k e to give our boys his child's hobbyhorse. When they Were tired of i t , wecould return it to him. That was 51ft enough for the little boys, but we let Herb invite anyone he wanted through
the vacation, and that was his present. Children should learn to appreciate their family's ability to give gifts, but they must also acceptsituations and enjoy what was given in love.
