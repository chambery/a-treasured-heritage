 Chapter 123
Herb, now in Chicago at 87th and Anthony, could attend the church where Dr. W. Masselink, my cousin Mary's husband, preached. Mary was his hostess many times, and under Bill's ministry he joined the church. Here he also met Birdie Koelkamp. Before
this assignment he was cmxduncoteague Island where the wild ponies are kept. He expected to ship out on an air carrier ship in the near future.
After the berry picking was finished wewent to Cleveland, Ohio, with Louis Roberts and G. Kuipers and alerted Herb to get leave and come there also. This wasall arranged; he would see
Ray Vanhof and Bob B a t t besides Louis and us.
The lady where Thomas and I were lodged very kindly suggested
that we shove the twin beds together so that we a l l three could sleep and talk the night away. Rev. Holtrop and his brother, also a minister, were the speakers at the conference.
When Herb had to return to Chicago, Thomas and I took him to the train station. Wecould see him only to the stairs because of war restrictions, and when he disappeared around t h e corner, Thomas and I felt as if he had left us forever, and it couldwell have
been. Men don't cry easily and neither do I, but now it was different. Herb and Ray came home from the seryice and planned to go to
Calvin College. They bought a tnailer and worked a l l summer to fi x it u p . Harry Vanhof made it so heavy with added aluminum and equipment they could scarcely p u l l it away, so they sold i t , and
Herb went o f f to Calvin without Ray.
That f a l l we had another blessing from our Father. Wep a i d o f f the farm; it was ours, and it seemed so wonderful to us to praise Him for this gift.

 This year the school board wanted a gegent certificate from the state. They sent in their request and the state an‐ swered t h a t t h e Commissioner would come to assess our school and decide if we qualified. He came and we a l l liked him at once even though he was our Judge. Weinvited him to our rooms, but he explained he'd stay in the hall. It was a l l the same, of course; the walls were f u l l of cracks, and what went on in the
rooms could plainly be heard in teh hall.
Thefirst dayhecameItoldhimoftheteachers'qualifi‐
cations starting with Mary, Doris, and then myself. I had no de‐ gree but credits f o r two years of college from Grundy Center and Calvin College, Iowa Falls State College, and where and how long my teaching career had been.
At the end of the week I asked him his opinion. His answer was, "I would never question your teaching, but you must con‐ tinue your education until you have a degree. Your library is not adequate." Likely he gave the same report to the Board, and they would get the Charter.
Mary Walcott would be married to Herbert Bird, studying
f o r mission work; Doris Jacobson would go to the Philippines as a teacher f o r missionaries' children; and Thomas and I had beguntornake other plans also.
The one event I remember distinctly in the last year was when a father came to our room and ordered h i s child out of the schoolroom. This boy had been staying with h i s grandmother so that he could attend church meeting in the evening. Wehad been taught at college that the teacher's authority superseded a parent's in the schoolroom. This power I had exerted a few times before in earlier years.

 I told the lad to stay in his seat and spoke to the father in the h a l l and explained the situation. In the school and on
the school property I, not he, had authority. ”You may wait in the car." He d i d f o r a while, and in those days heaters didn't work well even if he had one; eventually he lost the battle and the boy a beating.

