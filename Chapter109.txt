Chapter 109
We expected another child, Frances her first child, and Edna, Edward's wife also. If a l l went well our four children would have happy time with cousins as my mother and I did.
This time I was going to the hospital. Thomas had electrical work; the delivery and two weeks of care cost seventy dollars through the clinic, and we had that saved. My mother came before
t o h e l p m e g e t c l o t h e s r e a d y f o r t h e b a b y . Tommy was s t i l l i n
dresses but able to walk about. When he crawled, I washed the rugs each week to keephim clean, but now this was over.
Herb went each week to pay our winter debts; dentist, m i l k ‐ man, doctors for Tommy's birth, and the coal bill. Hecould ride the streetcar f o r fi v e cents and a penny transfer. Hewalked a mile to the carline and back home again. Hewas ten years old, gelf-reliant, and knew the state of our finances. Hereally didn't
think we needed another baby. Weshould be satisfied with Tommy whom he adored beyone measure. Tommy's fi r s t words were "Mama and Herb."
Mother was now sixty-eix years old and s t i l l very capable. She had always loved children, especially boys. Before she came to see us, she had seen CarhiJoanne, a surprise packagefor Emma
and John. She also told us that May Belle and Herman at long last were to have a child. They had been planning to adopt, and as is often the case when the process takes so long, she became preg‐ nant and proudly announced it whenever she could.
Our James Herman arrived July 26, 1955; Arlene on August 26, and Alma Lou on December 13, Herb's birthday. Each of these child‐ ren had physical problems. After a few months I noticed Jim's

 spine was curved, called scoliosis. He had two underdeveloped vertebrae, and at his first visit to the clinic it was defined. The doctors decided we should get state aid, and Jimmy would be p u t infia removable cast; each t h r e e months he should come and stay at the hospital for a larger cast to be made.
None of this happened because after I bathed him, I'd take him up by the shoulders and swing him ten times, then by his andkies and swing him again ten times. At his next month's check‐ up, Dr. Fush said, "It is surprising how his back has changed. What have you done?"
I hated to confess mytactics. ”Well, I swung him byhis shoulders and ankles each day ten times, and I asked a l l my friends from far and near to pray for him; I now believe he will
be at least near normal."
Dr. Bush said, "Maybe exercise w i l l be the cure." Jimmy
s t i l l had to go to the clinic every month. Then he had strange spells. I' d be watching him; we never held him but always laid
him on a covered board to help his back, and he would turn white, then blue, and become entirely 11mg; later he'd perspireand sleep for hours.
When I told the doctor that, he was put under a fluorescppe and they found he had pinprick lesions in his heart. The heart doctor to prepare me said, "Last week my wife and I had a crib~ bed death of our only child, and you may expect i t , too." This
news only brought us closer to God and made us more careful about Jim.
Jim outgrew h i s troubles; he was accepted in the Navy f o r
two years, had a back operation a few years ago, is an avid skier,

 and can do much physical work. He is a Ten Cate through and through.
Alma Lou, born last, had the worst troubles: a damaged heart f o r which she hadlaart surgery years a f t e r she had mar‐ ried John Hoogland. The day before the operation she passed her exams f o r library science; she also had another disease, M.S.,
which has remissions at times. She has had three brilliant children, is exceptionally bright herself, has several degrees, and is one of our favorite nieces and the cousins' favorite, too.
She and her husband, Major John, who has a spot in the Pentagon, l i v e near Washington, and she leads an almost normal l i f e .
Arlene married Alfred Rlatks.She was born with spinal bi‐ fida, but Dr. Smith against a l l hope tied the sack off, and fin‐
a l l y the hole healed. Today she bears only a rather pretty flower‐ like scar on her back. Her first child Daniel was adopted. A
year later she had Timmy, the most handsome of a l l the children, but he was injured at birth, and when he grew older was autistic and uncontrollable. He is Thomas M.'s age and in an institution, Danny had such dreadful experiences with "hoods" who stole h i s
wife that he died of a broken heart. Alfred and Arlene are de‐ voted Christians. The were also blessed with three girls; Martha,
married with a child, Priscilla with musical abilities; and Rachel. Margie married and had a child the same age as Dale, who is
now a lawyer. Stephan was also a brilliant boy as I hear h i s s i s ‐ ter Pamela i s . They visit us occasionally. Later I will write
more about the families of both sides--Thomas's and mine.
