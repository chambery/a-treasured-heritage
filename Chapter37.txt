 Chapter 37
That winter a missionary came to talk to the Orange City Church. He was young and fervent for his cause anl called upon parents to teach and dedicate t h e i r children to God's service.
Qedicate them before they were born, he saié. God knows just how long you w i l l keep them. He knows a l l their prospects in life long before they are born, to whom they will be given, and
Hegives a l l parents the privilege of teaching them the service of God, but parents must cultivate their own dedication first. Sostirring was his cry that a profound hush fell onthe
assembly, broken only by the announcement of a psalm. AS Julia and Jim rode home each with a child in their arms, they were silent. Only after the evening Scripture reading did Jim speak of what was in both.their hearts. ”Let us do as he said, dedi~
cate our children to the Lord. Maybe our next one w i l l be boy who will work for the Lord.”
”But they can't a l l be missionaries or ministers,“ said
no, said Jim, ”he didn't mean that. They can plow, maLe shoes, keep a store, keep house, j u s t about anything, and s t i l l
be dedicated."
”Our mother gave us a l l to the Lard again before she died;
she asked us always to stay together," said Julia, ”but see how Jan talks, and B i l l y fellows Hermanus in everythin;.”
"Well, it may depend onwhom they marry, and many people are sa ed just before they die. Your brothers may have been turned o f f by their stepfather, but yourmother's influence w i l l
he used by God someaay. It is in Go?I 8 hands in anv case. Ana .,
God saves the most unlikely people."
